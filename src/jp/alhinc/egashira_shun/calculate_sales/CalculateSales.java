package jp.alhinc.egashira_shun.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class CalculateSales {
	public static void main(String[] args) {
		try {
			//■1.支店定義ファイル読み込み
			Map<String, String> branchMap = new HashMap<>();//支店定義ファイル(支店コード・支店名)を管理する配列
			Map<String, Long> salesMap = new HashMap<>(); //売上ファイル(支店コード・売上額)を管理するmap配列


			//処理1-1: コマンドライン引数で指定されたディレクトリから支店定義ファイルを開く
			File branchFile = new File(args[0], "branch.lst");
			if (!branchFile.exists()) {
				//エラー処理1-1: 支店定義ファイルが存在しない場合
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			BufferedReader branchBr = null;
			try {
				//処理1-2: 支店定義ファイルを一行ずつデータを読み込み、全ての支店コードとそれに対応する支店名を保持する
				branchBr = new BufferedReader(new FileReader(branchFile));
				String sp;
				while ((sp = branchBr.readLine()) != null) {
					String[] branchData = sp.split(",");
					//エラー処理1-2: 支店定義ファイルのフォーマットが不正な場合(「支店名」はカンマ、改行を含まない文字列)
					if (branchData.length != 2) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					//エラー処理1-2: 支店定義ファイルのフォーマットが不正な場合(「支店コード」は数字のみ、3桁固定)
					if (!branchData[0].matches("^[0-9]{3}$")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					branchMap.put(branchData[0], branchData[1]);
					salesMap.put(branchData[0], 0L);
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					branchBr.close();
				} catch (Exception e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
			//■2.集計
			//処理2-1: コマンドライン引数で指定されたディレクトリから、拡張子がrcd、且つファイル名が数字8桁のファイルを検索
			File[] salesList = new File(args[0]).listFiles();
			ArrayList<String> salesFile = new ArrayList<String>();
			for (int i = 0; i < salesList.length; i++) {
				if (salesList[i].getName().matches("^[0-9]{8}+\\.rcd$")) {
					salesFile.add(salesList[i].getName());
				}
			}
			//エラー処理2-1: 売上ファイルが連番になっていない場合
			Collections.sort(salesFile);
			for (int i = 0; i + 1 < salesFile.size(); i++) {
				int num1 = Integer.parseInt(salesFile.get(i).substring(0, 8));
				int num2 = Integer.parseInt(salesFile.get(i + 1).substring(0, 8));
				if (num1 + 1 != num2) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			//処理2-2: 売上げファイルを読み込み、支店コード、売上額を抽出する
			BufferedReader salesBr = null;
			for (int i = 0; i < salesFile.size(); i++) {
				try {
					salesBr = new BufferedReader(new FileReader(new File(args[0], salesFile.get(i))));
					String branchCode = salesBr.readLine();//支店コードの抽出
					//エラー処理2-3: 支店に該当がなかった場合
					if (!branchMap.containsKey(branchCode)) {
						System.out.println(salesFile.get(i) + "の支店コードが不正です");
						return;
					}
					long branchSales = Long.parseLong(salesBr.readLine());//売上額の抽出
					//エラー処理2-4: 売上ファイルの中身が3行以上ある場合
					if (salesBr.readLine() != null) {
						System.out.println(salesFile.get(i) + "のフォーマットが不正です");
						return;
					}
					branchSales += salesMap.get(branchCode);
					//エラー処理2-2: 合計金額が10桁を超えた場合
					if (new Long(branchSales).toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					salesMap.put(branchCode, branchSales);
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					try {
						salesBr.close();
					} catch (Exception e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
			//■3.集計結果出力
			//処理3-1: コマンドライン引数で指定されたディレクトリに支店別集計ファイルを作成する
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(new File(args[0], "branch.out")));
				for (String key : branchMap.keySet()) {
					bw.write(key + ",");
					bw.write(branchMap.get(key) + ",");
					bw.write(salesMap.get(key).toString());
					bw.newLine();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					bw.close();
				} catch (Exception e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//エラー処理3-1: その他、処理内容に記載のないエラーメッセージ
		catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}